var testData = require('./testData');
var NameRegistry = require('origami-name-registry');
var ConnectedEmitters = require('origami-connected-emitters');
var Crane = require('..');
var assert = require('assert');

describe('crane2crane', function () {
  var socket1;
  var socket2;
  var nr1;
  var nr2;
  
  before(function () {
    var bus = new ConnectedEmitters();
    socket1 = bus.createEmitter();
    socket2 = bus.createEmitter();
  
    nr1 = new NameRegistry(testData.pair1.privateKey);
    nr2 = new NameRegistry(testData.pair2.privateKey);
  
    nr1.authorizeNamespace('n1', testData.pair2.publicKey);
    nr2.authorizeNamespace('n1', testData.pair2.publicKey);
    
  });
  
  var c1;
  var c2;
  var c1sockets = [];
  var c2sockets = [];
  
  it('creates cranes', function () {
    c1 = new Crane(
      nr1,
      function (socket) {
        c1sockets.push(socket);
        
        return Promise.resolve();
      },
      socket1
    );
    
    c2 = new Crane(
      nr2,
      function (socket) {
        c2sockets.push(socket);
        
        return Promise.resolve();
      },
      socket2
    );
  });
  
  var subsocket1;

  it('creates subsocket', function (done) {
    this.timeout(400);
    
    c2
    .createSocket(
      'n1',
      function (socket) {
        try {
          assert(socket);
          
          subsocket1 = socket;
          
          done();
        } catch (e) {
          done(e);
        }
        
        return Promise.resolve();
      }
    );
  });
  
  it('sends messages', function (done) {
    c1sockets[0].on('hi', done);
    subsocket1.emit('hi');
  });
});