var debug = require('debug')('origami:Crane');
var Penelope = require('origami-penelope');

function CraneOutgoingInitializer(nameRegistry, socketHandler) {
  return function (socket, params) {
    socket
    .on(
      'crane-challenge',
      function (challenge, callback) {
        try {
          var response = nameRegistry.answerChallenge(challenge);
          
          callback(null, response);
        } catch (e) {
          debug('crane answer challenge error %s', e);
          callback(e);
        }
      }
    );
    
    socket
    .on(
      'crane-challenge-accepted',
      function () {
        socketHandler(socket, params);
      }
    );
    
    return Promise.resolve();
  };
}

function CraneIncomingInitializer(nameRegistry, socketHandler) {
  return function (socket, params) {
    if (!socketHandler) return Promise.reject('not accepting incoming connectios');
    if (!params.namespace) return Promise.reject('namespace is required');
    
    try {
      if (!nameRegistry.isKnownNamespace(params.namespace)) return Promise.reject('unknown namespace');
    } catch (e) {
      return Promise.reject(e);
    } 
    
    var key = nameRegistry.getNamespacePublicKey(params.namespace);
    
    if (!key) {
      try {
        socketHandler(socket, params);
        
        socket.emit('crane-challenge-accepted');
      } catch (e) {
        return Promise.reject(e);
      }
    }
    
    var challenge = nameRegistry.createChallenge(params.namespace);
    
    socket
    .emit(
      'crane-challenge',
      challenge,
      function (err, response) {
        if (err) {
          debug('challenge error %s', err);
        }
        
        var verifies = nameRegistry.verifyChallenge(challenge, response);
        
        if (verifies) {
          try {
            socket.emit('crane-challenge-accepted');
            
            socketHandler(socket, params);
          } catch (e) {
            debug('crane incoming handler error %s', e);
          }
        }
      }
    );
    
    return Promise.resolve();
  };
}

function Crane (nameRegistry, incomingHandler, socket) {
  if (!nameRegistry) throw new Error('name registry is required');
  this.nameRegistry = nameRegistry;
  
  var penelope = this.penelope = new Penelope(socket);
  
  penelope
  .addHandler(
    'crane',
    new CraneIncomingInitializer(nameRegistry, incomingHandler)
  );
}

Crane.prototype.createSocket = function (namespace, initializer) {
  var self = this;
  
  return self
    .penelope
    .openSubsocket(
      new CraneOutgoingInitializer(
        self.nameRegistry,
        initializer
      ),
      'crane',
      {
        namespace: namespace
      }
    );
};

Crane.prototype.getNameRegistry = function () {
  return this.nameRegistry;
};

module.exports = Crane ;
