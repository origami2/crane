# Origami crane

### Purpose

To multiplex socket into subsockets and thus optimize lower connection levels.
Subsockets might want to connect to an specific namespace, and that namespace might require a secret challenge.

### Requirements

- [name-registry](https://gitlab.com/origami2/name-registry):
  Keeps a list of namespaces allowed to connect to this instance. Also, signs namespace requests for incoming connections.
  If the signature matches a subsocket connection is allowed to continue.
- initializer: delegate function that setups a subsocket after it was authenticated. It is expected to return a Promise.
- socket: undelaying socket that uses .emit/.on/.off API as an EventEmitter.

### Usage

listening incoming connections sockets

```javascript
var Crane = require('origami-crane');

var initializer = function (socket, namespace, crane) {
  socket
  .on(
    'sum',
    function (n1, n2, callback) {
      callback(n1 + n2);
    }
  );

  return Promise.resolve();
};

var nameRegistry; // see https://gitlab.com/origami2/name-registry/blob/master/README.md

var crane = new Crane(
  nameRegistry,
  intializer,
  socket
);
```

connecting to the first crane:

```javascript
var Crane = require('origami-crane');

var initializer = function (socket, namespace, crane) {
  // we won't accept incoming connections
  return Promise.reject();
};

var nameRegistry; // see https://gitlab.com/origami2/name-registry/blob/master/README.md

var crane = new Crane(
  nameRegistry,
  intializer,
  socket
);

crane.createSocket(
  'OtherSide', // namespace we want to connect to
  function (socket, namespace, crane) {
    return Promise.resolve(
      // this function will be called when the socket on both ends become ready
      function () {
      
      }
    );
  }
);
```